#include <iostream>
#include <Windows.h>
#include "CGame.h"
#include <conio.h>
#include <ctime>
#include <mmsystem.h>
using namespace std;
char MOVING;
CGame cg;
clock_t gameClockMinh;

bool IS_RUNNING = true;
void SubThread();
void FixConsoleWindow();
void resizeConsole(int width, int height);
void exitGame(thread* t);
void main()
{
	_setmode(_fileno(stdout), _O_U16TEXT);
	srand(time(NULL));
	int temp;
	FixConsoleWindow();
	resizeConsole(800, 600);

	//cg.startGame();
	thread t1;
	bool restart = false;
	bool saveLoad = false;
	int save = 0;
	int load = 0;
	do
	{
		if (!saveLoad)
		{
			if (save == 1)
			{
				cg.saveGame();
				save = 0;
			}
			else if (load == 1)
			{
				cg.loadGame();
				load = 0;
			}
			else
				cg.startGame();
		}
		t1 = thread(SubThread);
		while (true)
		{
			temp = toupper(_getch());
			if (!cg.getPeople()->isDead())
			{
				if (temp == 27) {
					cg.exitGame(&t1, IS_RUNNING);
					return;
				}
				else if (temp == 'P') {
					cg.pauseGame(t1.native_handle());

				}
				else if (temp == 'T')
				{


					cg.exitGame(&t1, IS_RUNNING);
					restart = true;
					IS_RUNNING = true;
					saveLoad = false;
					load = 1;
					break;
				}
				else if (temp == 'L')
				{
					cg.exitGame(&t1, IS_RUNNING);
					restart = true; //restart to make run new thread
					IS_RUNNING = true;
					saveLoad = false;
					save = 1;
					break; // break to restart
				}
				else {
					cg.resumeGame(t1.native_handle());
					MOVING = temp; //update move

				}
			}
			else
			{
				if (temp == 'Y') cg.resetGame(0);
				else {
					cg.exitGame(&t1, IS_RUNNING);
					return;

				}
			}
			saveLoad = false;
		}

	} while (restart);
	system("pause");


	//_setmode(_fileno(stdout), _O_U16TEXT);
	//int temp;
	//FixConsoleWindow();
	//resizeConsole(800, 600);
	//cg.startGame();
	//thread t1(SubThread);
	
	/*while (1)
	{
		
		temp = toupper(_getch());
		if (!cg.getPeople()->isDead())
		{
			if (temp == 27) {
				cg.exitGame(&t1,IS_RUNNING);
				return;

			}
		else if (temp == 'P') {
			cg.pauseGame(t1.native_handle());

			}
		else if (temp == 'T')
		{
			wcout.flush();//still need some fix for better
			system("cls");
			cg.pauseGame(t1.native_handle());
			cg.loadGame();
			cg.resumeGame(t1.native_handle());
		}
		else if (temp == 'L')
		{
			wcout.flush();//still need some fix for better
			system("cls");
			cg.pauseGame(t1.native_handle());
			cg.saveGame();
			cg.resumeGame(t1.native_handle());
		}
			else {
				cg.resumeGame(t1.native_handle());
				MOVING = temp; //update move

			}
		}
		else
		{
			if (temp == 'Y') cg.resetGame(0);
			else {
				cg.exitGame(&t1, IS_RUNNING);
				return;

			}
		}
	}*/
}
void SubThread()
{
	PlaySound(TEXT("Street SOUND EFFECTS - Street Ambience Hintergrundgeräusche Straße.wav"), NULL, SND_ASYNC);
	gameClockMinh = clock();
	while (IS_RUNNING) {
		double timeElapsed = clock() - gameClockMinh;
		gameClockMinh = clock();
		SetConsoleTitle((string("FPS: ") + std::to_string(1000.0/timeElapsed)).c_str());
		cg.drawMap();
		if (!cg.getPeople()->isDead()) //if still alive
		{
			cg.updatePosPeople(MOVING);//update move from main
		//}
			MOVING = ' ';// block and wait for user hit key
			cg.updatePosVehicle();//update pos vehicle
			cg.updatePosAnimal(); //update pos animal
			cg.drawGame();
		}
		if (cg.getPeople()->isImpact(cg.getVehicle()) ||
			cg.getPeople()->isImpact(cg.getAnimal()))
		{
			system("cls");
			wcout << "\n\n\n\t\t\tYou lose" << endl;
			wcout << "\t\t\tPress y if you want to play again." << endl;

		}
		if (cg.getPeople()->isFinish()) {
			cg.levelup(IS_RUNNING);

		}
		Sleep(1);
	}
}
void FixConsoleWindow()
{
	HWND consoleWindow = GetConsoleWindow();
	LONG style = GetWindowLong(consoleWindow, GWL_STYLE);
	style = style & ~(WS_MAXIMIZEBOX) & ~(WS_THICKFRAME);
	SetWindowLong(consoleWindow, GWL_STYLE, style);
}
void resizeConsole(int width, int height)
{
	HWND console = GetConsoleWindow();
	RECT r;
	GetWindowRect(console, &r);
	MoveWindow(console, r.left, r.top, width,height, TRUE);
}
void exitGame(thread* t) {
	ClearScreen();
	IS_RUNNING = false;
	t->join();
}
